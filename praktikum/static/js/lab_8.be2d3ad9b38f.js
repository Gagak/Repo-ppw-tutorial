// FB initiation function
window.fbAsyncInit = function() {
  FB.init({
    appId      : '1739683079383485',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  console.log("FB INIT");
  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)

  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
   FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          render(true);
        } else {
          render(false);
        }

    });
};

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Rubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      $('#fblogin').remove();
      // Jika yang akan dirender adalah tampilan sudah login
      // Panggil Method getUserData yang anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<img style="margin: 0px auto;display:block;" class="cover" src="' + user.cover.source + '" alt="cover" />' +'<br><br>'+
            '<div class = "container">'+
            '<div class = "row">'+
              '<div class = "col-xs-2">'+
            '<img style="height:100px;width=100px;border-radius:50%;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '</div>'+
            ' <div class="col-xs-10">'+
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
            '</div>'+'</div>'+'</div>'+'<br>'+
            '<h2>About Me</h2><br>'+'<p style="margin-left:20px;">' + user.about + '</p>' +
          '</div><br>' +
          '<h2>Post ke Facebook</h2><br>'+
          '<textarea id="postInput" type="text" class="post" placeholder="Ketik Status Anda"></textarea><br>' +
          '<button style="margin-left:20px;background-color: blue;color: white;" class="postStatus" onclick="postStatus()">Kirim</button>' +
          '<br><h2>Timeline</h2><br>'
        );

        // Setelah merender tampilan diatas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          console.log("getUserFeed")
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            //jika terdapat fields message dan story pada feed
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed" style="position: relative;" >' +
                  '<p>' + value.message + '</p>' +
                  '<p>' + value.story + '</p>' +'<span onclick="deletePost(\'' + value.id + '\')" data-id="'+value.id+'" class="glyphicon glyphicon-remove" style="color: red;float : right;position: absolute;top: 0;right: 0;"></span>'+
                '</div>'
              );
              //jika hanya ada field message pada feed
            } 
            else if (value.message) {
              $('#lab8').append(
                '<div class="feed" style=" position: relative;" >' +
                '<div class="row">'+
                '<div class="col-xs-1">'+
                '<img style="height:50px;width=50px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" /></div>'+
                '<div class="col-xs-10"><div class="row"><div class="row-xs-6 name">'+
                '<p>' + user.name + '</p></div>'+
                '<div class="row-xs-6 date">'+
                '<p>' + value.created_time + '</p></div></div></div> <div class="col-xs-1"></div></div>'+
                '<div class="row"><div class="col-xs-12 status-field">'+
                  '<p>' + value.message + '</p>'+ 
                '</div>'+
                '</div>'+'<span onclick="deletePost(\'' + value.id + '\')" data-id="'+value.id+'" class="glyphicon glyphicon-remove" style="color: red;float : right;position: absolute;top: 0;right: 0;"></span>'+
                '</div>'
              );
              //jika hanya ada story pada feed
            } 
            else if (value.story) {
              //jika story memiliki gambar dan deskripsi
              if(value.description && value.picture){
              $('#lab8').append(
                '<div class="feed" style=" position: relative;" >' +
                '<div class="row">'+
                '<div class="col-xs-1">'+
                '<img style="height:50px;width=50px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" /></div>'+
                '<div class="col-xs-10"><div class="row"><div class="row-xs-6 name">'+
                '<p>' + value.story +'<a href="'+value.link+'" >(link)</a>'+ '</p></div>'+
                '<div class="row-xs-6 date">'+
                '<p>' + value.created_time + '</p></div></div></div> <div class="col-xs-1"></div></div>'+
                '<div class="row"><div class="col-xs-12 status-field">'+
                  '<p>' + value.description + '</p>'+
                    '<img style="margin: 0px auto;display:block;" class="picture" src="' + value.picture + '" alt="pic" />'+
                '</div>'+
                '</div>'+'<span onclick="deletePost(\'' + value.id + '\')" data-id="'+value.id+'" class="glyphicon glyphicon-remove" style="color: red;float : right;position: absolute;top: 0;right: 0;"></span>'+
                '</div>'
              );
              //jika story hanya memiliki field gambar (biasanya saat update foto profil)
              }
              else if(value.picture){
                  $('#lab8').append(
                    '<div class="feed" style=" position: relative;" >' +
                    '<div class="row">'+
                    '<div class="col-xs-1">'+
                    '<img style="height:50px;width=50px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" /></div>'+
                    '<div class="col-xs-10"><div class="row"><div class="row-xs-6 name">'+
                    '<p>' + value.story + '</p></div>'+
                    '<div class="row-xs-6 date">'+
                    '<p>' + value.created_time + '</p></div></div></div> <div class="col-xs-1"></div></div>'+
                    '<div class="row"><div class="col-xs-12 status-field">'+
                      '<img style="margin: 0px auto;display:block;" class="picture" src="' + value.picture + '" alt="pic" />'+ 
                    '</div>'+
                    '</div>'+'<span onclick="deletePost(\'' + value.id + '\')" data-id="'+value.id+'" class="glyphicon glyphicon-remove" style="color: red;float : right;position: absolute;top: 0;right: 0;"></span>'+
                    '</div>'
                  );
              //jika story hanya memiliki field deskripsi (biasanya saat share suatu tautan/link)
              }
              else if(value.description){
                $('#lab8').append(
                  '<div class="feed" style=" position: relative;" >' +
                  '<div class="row">'+
                  '<div class="col-xs-1">'+
                  '<img style="height:50px;width=50px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" /></div>'+
                  '<div class="col-xs-10"><div class="row"><div class="row-xs-6 name">'+
                  '<p>' + value.story + '<a href="'+value.link+'" >(link)</a>'+'</p></div>'+
                  '<div class="row-xs-6 date">'+
                  '<p>' + value.created_time + '</p></div></div></div> <div class="col-xs-1"></div></div>'+
                  '<div class="row"><div class="col-xs-12 status-field">'+
                    '<p>' + value.description + '</p>'+
                  '</div>'+
                  '</div>'+'<span onclick="deletePost(\'' + value.id + '\')" data-id="'+value.id+'" class="glyphicon glyphicon-remove" style="color: red;float : right;position: absolute;top: 0;right: 0;"></span>'+
                  '</div>'
                );
                //jika story tidak memiliki field gambar dan deskripsi (biasanya saat seseorang mengirimkan pesan pada dinding)
              }
              else{
                $('#lab8').append(
                  '<div class="feed" style=" position: relative;" >' +
                  '<div class="row">'+
                  '<div class="col-xs-1">'+
                  '<img style="height:50px;width=50px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" /></div>'+
                  '<div class="col-xs-10"><div class="row"><div class="row-xs-6 name">'+
                  '<p>' + value.story + '</p></div>'+
                  '<div class="row-xs-6 date">'+
                  '<p>' + value.created_time + '</p></div></div></div> <div class="col-xs-1"></div></div>'+
                  '<div class="row"><div class="col-xs-12 status-field">'+
                     
                  '</div>'+
                  '</div>'+'<span onclick="deletePost(\'' + value.id + '\')" data-id="'+value.id+'" class="glyphicon glyphicon-remove" style="color: red;float : right;position: absolute;top: 0;right: 0;"></span>'+
                  '</div>'
                );
              }
            }
          });
          //tombol logout
          $('#lab8').append(
          '<button class="logout" onclick="facebookLogout()">Logout</button><br>');
          //tampilkan tombol untuk mengganti tema
        });
      });
    } 
    else {
      // Tampilan ketika belum login
      $('#lab8').html("<form id= 'fblogin' class='modal-content'>"+
      "<div class='imgcontainer'>" +
      "<img src='https://cdn.worldvectorlogo.com/logos/facebook-3.svg' alt='Avatar' class='avatar'>"+
      "</div>" +
      "<div class='container'>" +
      "<h1>Hello " + 'Haikal Ravendy' +"</h1>" +
      "<h2>Please login with your facebook below</h2>" +
      "<button onclick='facebookLogin()'>Login</button>" +
      "</div>" +
      "</form>"
      )   
    };  
  };
  
  const facebookLogin = () => {
    console.log("FB Login");
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
       console.log(response);
       render(true);
     }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me,publish_pages,user_managed_groups'})
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          render(false);
        }

     });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  //fungsi callback adalah yang dijalankan didalam fungsi lain ketika fungsi tersebut selesai di eksekusi
  // fungsi callback bernama fun diganti nama menjadi getUserData
  const getUserData = (getUserData) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name,cover,picture.type(large),about,email,gender', 'GET', function(response){
            console.log(response);
            getUserData(response);
          });
        }
    });
  };

  const getUserFeed = (getUserFeed) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me/feed?fields=story,message,full_picture,link,description,caption,name,picture,attachments,created_time', 'GET', function(response){
            console.log(response);
            getUserFeed(response);

          });
        }
    });
  };

  const postFeed = (status) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
     var message = status;
     FB.api('/me/feed', 'POST', {message:message});
     render(true)

  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };

  const deletePost = (id) => {
    FB.api('/'+id, 'DELETE',function(response){
      console.log(response);
      if(response.success){
        render(true);  
      }
      else{
        alert("Tidak dapat menghapus post yang tidak di post melalui aplikasi lab 8!");
      }
      
    });
  };
