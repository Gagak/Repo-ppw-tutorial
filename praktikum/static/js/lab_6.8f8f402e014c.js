$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
$(".chat-text").keypress(function(e) {
    //cek apakah key yang ditekan tombol Enter atau bukan
    if(e.which == 13) {
    	e.preventDefault(); //default event tombol enter tidak dilakukan
    	var inp = $("textarea").val(); //ambil data di textarea
      $("textarea").val(""); //menghapus text area
      $(".msg-insert").append('<p class="msg-send">'+inp+'</p>'+'<br/>'); //munculkan text di body chat
    }
	
	});

    $(".chat-button").click(function(){
      var inp = $("textarea").val(); //ambil data di textarea
      $("textarea").val(""); //menghapus text area
        $(".msg-insert").append('<p class="msg-send">'+inp+'</p>'+'<br/>'); //munculkan text di body chat
    });
});

 //inisiasi data tema
  var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
  
  //default theme jika web pertama kali dibuka
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  //simpan data tema di local storage
  localStorage.setItem('themes', JSON.stringify(themes));
	
  //menload tema ke select2
	var retrievedObject = localStorage.getItem('themes');
	$('.my-select').select2({data: JSON.parse(retrievedObject)});
	
  //menerapkan selectedTheme yang ada di local storage
  var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
    var key;
    var bcgColor;
    var fontColor;
	for (key in retrievedSelected) {
    	if (retrievedSelected.hasOwnProperty(key)) {
        	bcgColor=retrievedSelected[key].bcgColor;
        	fontColor=retrievedSelected[key].fontColor;
    	}
	}  
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color":fontColor});


  //fungsi tombol apply
	$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var valueTheme = $('.my-select').val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    var theme;
    var a;
    var selectedTheme = {};
    //mencari tema yang sesuai dengan id
    for(a in themes){
    	if(a==valueTheme){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});







// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
    if (x === 'ac') {
        print.value = '0';
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } else if(x === 'log'){
    	print.value = parseFloat(Math.log10(evil(print.value))).toFixed(1);
    	erase = true ;
    }
    else if(x === 'sin'){
    	print.value += parseFloat(Math.sin((evil(print.value)/180)*Math.PI)).toFixed(4);
    	erase = true ;
    }else if(x === 'tan'){
    	print.value = parseFloat(Math.tan((evil(print.value)/180)*Math.PI).toFixed(4));
    	erase = true ;
    }else {
        if (print.value === '0' && x !== "0") {
            print.value = x;
            erase = false;
        }
        else if(erase===true&&(x!==' * '&&x!==' - '&&x!==' + '&&x!==' / ')){
    	go('ac');
	}
		else{
			print.value += x;
			erase = false;
	}

    }
};



function evil(fn) {
    return new Function('return ' + fn)();
}
// END


//ChatBox
/**var box = document.getElementById("result");
var chat = function(){
	
		var nilai = box.innerHTML.split(" ");
		if(nilai.length = 0){
			box.innerHTML = document.getElementById("nameField").value;
		}
		else{
			document.getElementById("msg-insert").append('<p class="msg-send">'+box.innerHTML+'</p>');
			box.innerHTML += "<br>" + document.getElementById("nameField").value;
			document.getElementById("nameField").value= "";
		}
	
};**/
//END