from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
import requests
from .api_entercomputer import get_drones, get_soundcards, get_optical
from .csui_helper import get_access_token, verify_user, get_client_id , get_data_user
import environ
from django.urls import reverse


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
# Create your tests here.
class Lab9UnitTest(TestCase):

	def test_lab_9_url_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code, 200)

	def test_lab9_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	def test_lab9_using_right_template(self):
		#jika belum login
		response = self.client.get('/lab-9/')
		self.assertTemplateUsed(response, 'lab_9/session/login.html')
		#login
		session = self.client.session
		session['user_login'] = 'test'
		session['kode_identitas'] = '123'
		session.save()
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code, 302)

	def test_profile(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		response = self.client.get('/lab-9/profile/')
		html_response = response.content.decode('utf8')
		self.assertIn('haikal.ravendy',html_response)

	def test_profile_not_login(self):
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

	
	############################################################################
	# Test api_enterkomputer.py
	def test_drones_api(self):
		response = requests.get('https://www.enterkomputer.com/api/product/drone.json')
		self.assertEqual(response.json(),get_drones().json())

	def test_soundcards_api(self):
		response = requests.get('https://www.enterkomputer.com/api/product/soundcard.json')
		self.assertEqual(response.json(),get_soundcards().json())

	def test_optical_api(self):
		response = requests.get('https://www.enterkomputer.com/api/product/optical.json')
		self.assertEqual(response.json(),get_optical().json())

	#############################################################################
	# Test csui_helper

	def test_verify_function(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		access_token = get_access_token(self.username,self.password)
		parameters = {"access_token": access_token, "client_id": get_client_id()}
		response = requests.get(API_VERIFY_USER, params=parameters)
		result = verify_user(access_token)
		self.assertEqual(result,response.json())

	
	#################################################################################
	# Test custom_auth
	def test_login_auth(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		self.assertEqual(response_post.status_code, 302)

	def test_fail_login(self):
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': 'aku', 'password': 'ganteng'})
		response = self.client.get('/lab-9/')
		html_response = response.content.decode('utf8')
		self.assertIn('Username atau password salah',html_response)

	def test_logout_auth(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")
		response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
		response = self.client.post(reverse('lab-9:auth_logout'))
		response = self.client.get('/lab-9/')
		html_response = response.content.decode('utf8')
		self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

		# ======================================================================== #
